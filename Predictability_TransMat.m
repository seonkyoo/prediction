

function [T_U, T_D, T_L, T_R, T_F, T_B, Lmat, Lmat_transpose, invDmat, perm] =...
    Predictability_TransMat(lmat,Grid)

    nx=Grid.cartDims(1); ny=Grid.cartDims(2); nz=Grid.cartDims(3);    %% No of cell in each direction
    dx=Grid.cells.centroids(1,1)*2; 
    dy=Grid.cells.centroids(1,2)*2; 
    dz=Grid.cells.centroids(1,3)*2;       %% Cell size [m]


    Tz = 2./( 1./lmat(2:nz,:,:) + 1./lmat(1:nz-1,:,:) ) * dx*dy/dz;
    Tx = 2./( 1./lmat(:,2:nx,:) + 1./lmat(:,1:nx-1,:) ) * dy*dz/dx;
    Ty = 2./( 1./lmat(:,:,2:ny) + 1./lmat(:,:,1:ny-1) ) * dz*dx/dy;

    T_U = cat(1, zeros(1,nx,ny), Tz); TU=T_U(:); % no flow B.C. at the top edge
    T_D = cat(1, Tz, zeros(1,nx,ny)); TD=T_D(:); % no flow B.C. at the bottom edges
    T_L = cat(2, zeros(nz,1,ny), Tx);  TL=T_L(:); % const flux B.C at the inlet (left)  
    T_R = cat(2, Tx, 2*dz*dy/dx*lmat(:,nx,:));  TR=T_R(:); % hydrostatic B.C. at the outlet, assuming inf Tx at outlet
    T_F = cat(3, zeros(nz,nx,1), Ty); TF=T_F(:); % no flow B.C. at the top edge
    T_B = cat(3, Ty, zeros(nz,nx,1)); TB=T_B(:); % no flow B.C. at the bottom edges

    T = spdiags([-TB -TR -TD (TL+TR+TD+TU+TB+TF) -TU -TL -TF],...
                              [-nz*nx,-nz,-1,0,1,nz,nz*nx],nz*nx*ny,nz*nx*ny)';

    [Lmat, Dmat, perm] = ldl(T,'vector');
    Lmat_transpose = Lmat';
    invDmat = 1./diag(Dmat);
    
end
