
clear all; close all; clc
load(['GeoModel.mat'],'rock','fluid','Grid');
nx=Grid.cartDims(1); ny=Grid.cartDims(2); nz=Grid.cartDims(3); % #. of cell in each direction
Perm = rock.perm(:,1);

figure;
imagesc(reshape(log(Perm),nz,nx,ny))
colorbar; colormap('jet'); axis equal tight
title('Gaussian log-permeablility field')

%% Transmissibility Matrix
mu = fluid.mu; phi = fluid.phi;

TE=cell(size(Perm,2),1);
for j = 1 : size(Perm,2)
    [TE{j}.U, TE{j}.D, TE{j}.L, TE{j}.R, TE{j}.F, TE{j}.B, ...
        TE{j}.Lmat,TE{j}.Lmat_transpose,TE{j}.invDmat,TE{j}.perm] = ...
            Predictability_TransMat(reshape(Perm(:,j),nz,nx,ny)./mu./phi,Grid);
end

%% Simulation
figure;
[t,XE,ProW,ProC,dts] = Predictability_sim(-1,0,fluid,Grid,TE); 

figure;
plot(cell2mat(dts)/(24*60*60),cell2mat(ProC)./cell2mat(ProW))
xlabel('day');
ylabel('concentration');
title('breakthrough curve');