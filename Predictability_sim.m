%% ------------------------------------------------------------------------
% DESCRIPTION:
%   Simulate synthetic truth and ensemble models to the next update time or 
%   to the specified end time and store time series of observation .
%--------------------------------------------------------------------------

function [t,XE,ProWE,ProCE,dtsE] = ...
    Predictability_sim(M,t,fluidE,Grid,TE)

    tu = abs(M)*120;
    dt = tu;

    nx=Grid.cartDims(1); ny=Grid.cartDims(2); nz=Grid.cartDims(3);    %% No of cell in each direction
    dx=Grid.cells.centroids(1,1)*2; 
    dy=Grid.cells.centroids(1,2)*2; 
    dz=Grid.cells.centroids(1,3)*2;       %% Cell size [ft]
    Lx = nx*dx; Ly = ny*dy; Lz = nz*dz;


    [xx,~,~] = meshgrid(dx/2:dx:Lx-dx/2,dz/2:dz:Lz-dz/2,dy/2:dy:Ly-dy/2);
    c0= 1/2*( tanh(1*(xx-0.01*Lx)) )+0.5; % smooth initial concentration profile
    c0=1-c0; c0 = flip(c0,2);
    
     % Ensemble
     XE=cell(length(TE),1);
     for j = 1 : length(TE)
         XE{j}.c=c0;
     end


   nmembers=length(XE);
   % simulate true (FOM) + ensemble (FOM) ---------------------------------
   txt=sprintf('|   * Simulate %d Ensemble',nmembers);
   disp(txt); %fprintf(fid,[txt '\r\n']);
   
   ProWE=cell(nmembers,1); ProCE=cell(nmembers,1); dtsE = cell(nmembers,1);   
   
   i=0;
   while abs(t - tu) >0.0001
      i=i+1;
      % Truth
%       [X,~,ProW,ProC,dts] = simForward(M,Grid,fluid,X,T,t,dt);
      % Ensemble 
      for j = 1 : nmembers
           [XE{j},~,ProWE{j},ProCE{j},dtsE{j}] = simForward(M,Grid,fluidE(j),XE{j},TE{j},t,dt);
      end

      t = t + dt;

      txt = sprintf('|     - %.3f ~ %.3f', t-dt,t);
      disp(txt); 
      
   end

end


%% ------------------------------------------------------------------------
% DESCRIPTION:
%   Evolve state variables (pressure & concentration) upto next time step
%--------------------------------------------------------------------------  

function [X,t,proW,proC,dts] = simForward(M,Grid,fluid,X,T,t,dt)
    tf = (t+dt) * 24 * 60 * 60; clear dt;
    t = t * 24 * 60 * 60;
    
    nx=Grid.cartDims(1); ny=Grid.cartDims(2); nz=Grid.cartDims(3);  
    dx=Grid.cells.centroids(1,1)*2; 
    dy=Grid.cells.centroids(1,2)*2; 
    dz=Grid.cells.centroids(1,3)*2;       %% Cell size [m]
    g = fluid.g;
    
    beta = fluid.beta;
    if M > 0
        rho0 = fluid.rho0;
        rho1 = fluid.rho1;
        DelRho = rho1-rho0;
        Uin = exp(fluid.mlogk)*fluid.DelRho*g /M/fluid.mu;
    elseif M < 0
        rho0 = fluid.rho0;
%         rho0 = fluid.rho1;
%         rho1 = fluid.rho0;
        rho1 = fluid.rho1;
%         DelRho = 0;
        DelRho = fluid.DelRho;
        Uin = exp(fluid.mlogk)*fluid.DelRho*g /M/fluid.mu;
    end
%     g=0;
    Phydro = repmat(rho1*g*dz*[1:nz]',1,ny); % hydrostatic pressure at the right boundary
    Phydro = reshape(Phydro, nz,1,ny);
    
    proWR = []; proCR=[]; 
    proW = []; proC=[]; dts = []; itmp = 0;
    while abs(tf - t) >0.00001 
        itmp = itmp+1;
        %% pressure field
        c0 = reshape(X.c(:),nz,nx,ny);
        c0_avg = cat(1, zeros(1,nx,ny), (c0(1:end-1,:,:)+c0(2:end,:,:))/2, zeros(1,nx,ny));


        b = + T.U.*(rho0+DelRho*c0_avg(1:end-1,:,:))*g*dz ...
            - T.D.*(rho0+DelRho*c0_avg(2:end,:,:))*g*dz;

        b(:,1,:) = b(:,1,:) + Uin*dz*dy;
        b(:,end,:) = b(:,end,:) + T.R(:,end,:).* Phydro;

        bvec = b(:);

        pvec(T.perm,:)=T.Lmat_transpose\(T.invDmat.*(T.Lmat\(bvec(T.perm,:))));

        pmat=reshape(pvec,nz,nx,ny);


        %% Transport (advection)
        Pzdif=[pmat(2:end,:,:)-pmat(1:end-1,:,:)]-(rho0+DelRho*c0_avg(2:end-1,:,:))*g*dz;
        Pxdif=[pmat(:,2:end,:)-pmat(:,1:end-1,:)];
        Pydif=[pmat(:,:,2:end)-pmat(:,:,1:end-1)];

        Pxdif_L = cat(2, zeros(nz,1,ny), Pxdif); 
        Pxdif_R = cat(2, Pxdif, Phydro-pmat(:,end,:)); 
        Pzdif_U = cat(1, zeros(1,nx,ny), Pzdif);
        Pzdif_D = cat(1, Pzdif, zeros(1,nx,ny));
        Pydif_F = cat(3, zeros(nz,nx,1), Pydif);
        Pydif_B = cat(3, Pydif, zeros(nz,nx,1));

        U_L = -T.L/dz/dy.*Pxdif_L;     U_L(:,1)=Uin;
        U_R = -T.R/dz/dy.*Pxdif_R;
        U_U = -T.U/dx/dy.*Pzdif_U;
        U_D = -T.D/dx/dy.*Pzdif_D;
        U_F = -T.F/dz/dx.*Pydif_F;
        U_B = -T.B/dz/dx.*Pydif_B;

        c0_L = cat(2, zeros(nz,1,ny), c0(:,1:end-1,:));  
        c0_R = cat(2, c0(:,2:end,:), ones(nz,1,ny));    
        c0_U = cat(1, zeros(1,nx,ny), c0(1:end-1,:,:));       
        c0_D = cat(1, c0(2:end,:,:), zeros(1,nx,ny));
        c0_F = cat(3, zeros(nz,nx,1), c0(:,:,1:end-1));
        c0_B = cat(3, c0(:,:,2:end), zeros(nz,nx,1));

        e0=zeros(nz,nx,ny); 
        Fadv_L = e0; Fadv_R = e0; 
        Fadv_D = e0; Fadv_U = e0; 
        Fadv_F = e0; Fadv_B = e0; 

        Fadv_L(U_L>=0) = U_L(U_L>=0) .* c0_L(U_L>=0)*dz*dy; 
        Fadv_L(U_L<0)  = U_L(U_L<0)  .* c0(U_L<0)   *dz*dy; 
        Fadv_R(U_R>=0) = U_R(U_R>=0) .* c0(U_R>=0)  *dz*dy;
        Fadv_R(U_R<0)  = U_R(U_R<0)  .* c0_R(U_R<0) *dz*dy;
        Fadv_U(U_U>=0) = U_U(U_U>=0) .* c0_U(U_U>=0)*dx*dy; 
        Fadv_U(U_U<0)  = U_U(U_U<0)  .* c0(U_U<0)   *dx*dy; 
        Fadv_D(U_D>=0) = U_D(U_D>=0) .* c0(U_D>=0)  *dx*dy;
        Fadv_D(U_D<0)  = U_D(U_D<0)  .* c0_D(U_D<0) *dx*dy;
        Fadv_F(U_F>=0) = U_F(U_F>=0) .* c0_F(U_F>=0)*dz*dx;
        Fadv_F(U_F<0)  = U_F(U_F<0)  .* c0_F(U_F<0) *dz*dx;
        Fadv_B(U_B>=0) = U_B(U_B>=0) .* c0_B(U_B>=0)*dz*dx;
        Fadv_B(U_B<0)  = U_B(U_B<0)  .* c0_B(U_B<0) *dz*dx;

        Fadv = (-Fadv_F -Fadv_L - Fadv_U + Fadv_D + Fadv_R + Fadv_B);    

        %% Transport (Diffusion)
        Fdif_L = (10^-9+beta*abs(U_L)).*cat(2, zeros(nz,1,ny), c0(:,2:end,:)-c0(:,1:end-1,:))/dx*dz*dy; 
        Fdif_R = (10^-9+beta*abs(U_R)).*cat(2, c0(:,2:end,:)-c0(:,1:end-1,:), zeros(nz,1,ny))/dx*dz*dy; 
        Fdif_U = (10^-9+beta/10*abs(U_U)).*cat(1, zeros(1,nx,ny), c0(2:end,:,:)-c0(1:end-1,:,:))/dz*dx*dy;
        Fdif_D = (10^-9+beta/10*abs(U_D)).*cat(1, c0(2:end,:,:)-c0(1:end-1,:,:), zeros(1,nx,ny))/dz*dx*dy; 
        Fdif_F = (10^-9+beta/10*abs(U_F)).*cat(3, c0(:,:,2:end)-c0(:,:,1:end-1), zeros(nz,nx,1))/dy*dz*dx; 
        Fdif_B = (10^-9+beta/10*abs(U_B)).*cat(3, zeros(nz,nx,1), c0(:,:,2:end)-c0(:,:,1:end-1))/dy*dz*dx; 

        Fdif = (-Fdif_F -Fdif_L - Fdif_U + Fdif_D + Fdif_R + Fdif_B); 

        %% Advance in Time
        umax = max(...
            max(max(max(abs(cat(2, U_L, U_R(:,end,:)))))), ...
            max(max(max(abs(cat(1, U_U, U_D(end,:,:))))))  ...
            );
        dt_adv=.1*dy/umax;
        Dmax = max  (...
            max(max(max(10^-9+beta*abs(cat(2, U_L, U_R(:,end,:)))))), ...
            max(max(max(10^-9+beta*abs(cat(1, U_U, U_D(end,:,:))))))  ...
            );
        dt_diff=0.1*dx*dx/Dmax; % diffusive time scale

        dt=min(dt_adv,dt_diff);

%         dt = dt * 24 * 60 * 60;
        if t+dt >= tf
            dt = tf - t;
        end
        
        t=t+dt;
        c0 = c0 - dt/(dx*dz*dy)*(Fadv-Fdif);

        X.c = c0(:);
        X.p = pmat(:);
        
        proW = [proW; sum(U_L(:,1))*fluid.phi*dz*dy*dt*24*60*60];
        proC = [proC; sum(Fadv_L(:,1)+Fdif_L(:,1))*fluid.phi*dt*24*60*60];
        dts = [dts; dt];
        if mod(itmp,300)==0
            clf; 
            subplot(3,2,1:2)
            imagesc(c0); colorbar; colormap('jet'); caxis([0 1])
            title(['concentration field']);
            subplot(3,2,3:4)
            imagesc(pmat); colorbar; colormap('jet')
            title('pressure field')
            subplot(3,2,5)
            plot(cumsum(dts)/24/60/60,-proW./dts); ylim([36 40])
            title('Production rate [m^3/day]')
            subplot(3,2,6)
            plot(cumsum(dts)/24/60/60,proC./proW); ylim([-0.1 1])            
            title('breakthrough Curve')
            suptitle(['t = ' num2str(t / 24 / 60 / 60) ' d']);
            drawnow
            
        end
    end
    
    extractInd = [1:floor(length(dts)/1200):length(dts)]; 
    proW = proW(extractInd);
    proC = proC(extractInd);
    dts = cumsum(dts);
    dts = dts(extractInd);
    
    
    
    t = tf / (24 * 60 * 60);
end
